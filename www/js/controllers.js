app.controller('AppCtrl', function($scope, UserDetails) {
	$scope.user = {};	
})

.controller('WelcomeCtrl', function($scope, $state, UserDetails) {
  $scope.user= {};
  $scope.onSubmit = function(user) {
   	UserDetails.setUser(user);
    $state.go('verify');
  }
})


.controller('VerifyCtrl', function($scope, $state, Config) {
  
  $scope.user = {};

  $scope.onSubmit = function(user) {
  	localStorage.setItem(Config.localStorageKeyword, "true");
    $state.go('app.latest_news');
  }
})

.controller('NewsListCtrl', function($scope, Fetch, 
		$ionicLoading, $state, $ionicHistory, 
		$ionicPopup) {
	$ionicHistory.clearHistory()
	var nlc = this;
	$ionicLoading.show();
	nlc.doRefresh = function() {
		fetchLatestNews();	
	}

   	fetchLatestNews();	
	
	function fetchLatestNews(){
		
		Fetch.top20news().success(function(data) {
			nlc.data = data.story;
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
		}).error(function(error) {
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
			$ionicPopup.show({
   			template : '<p>Unable to fetch data. Please try again</p>',
   			title: 'Error',
   			scope: $scope,
   			buttons: [
		      { text: 'Cancel' },
		      {
		        text: '<b>Try Again</b>',
		        type: 'button-balanced',
		        onTap: function() {
		            again();
		        }
		      }
    		]
   		});	
			console.log(JSON.stringify(error))
		})
	}

	nlc.viewDetail = function(data) {
		$state.go("news_detail", {details: JSON.stringify(data)});
	}

	function again(){
		$ionicLoading.show();
		fetchLatestNews();
	}
})

.controller('CategoriesCtrl', function($scope, Fetch, $ionicLoading, $state, $ionicPopup) {
	var cctrl = this;
	fetchCategories();

	function fetchCategories() {
		$ionicLoading.show()
		Fetch.getCategoriesList().success(function(data) {
			cctrl.items = data.category;
			$scope.isDataAvailable = true;
			$ionicLoading.hide();
		}).error(function(error) {
			$ionicLoading.hide();
			$ionicPopup.show({
   			template : '<p>Unable to fetch data. Please try again</p>',
   			title: 'Error',
   			scope: $scope,
   			buttons: [
		      { text: 'Cancel' },
		      {
		        text: '<b>Try Again</b>',
		        type: 'button-balanced',
		        onTap: function() {
		            again();
		        }
		      }
    		]
   		});	
			console.log(JSON.stringify(error))
		})
	}

	function again(){
		fetchCategories();
	}

	cctrl.goToSubcategories = function(data){
		console.log(data.name)
		$state.go("subcategories", {id: data.id, name: data.name})
	}

})

.controller('NewsDetailCtrl', function($scope, $state, $stateParams, 
				$ionicHistory, $ionicPopup) { 
	
	$scope.goBack = function() {
      	$ionicHistory.goBack();
    };

   	$scope.detail = JSON.parse($stateParams.details);

   	$scope.ratingCallback = function(rating, index) {
	        console.log('Selected rating is : ', rating, ' and the index is : ', index);
	      };

   	
   	$scope.showRatingPopUp = function() {
   		$scope.ratingsObject =  {
    	    iconOn: 'ion-ios-star',    //Optional
    	    iconOff: 'ion-ios-star-outline',   //Optional
    	    iconOnColor: 'rgb(200, 200, 100)',  //Optional
    	    iconOffColor:  'rgb(200, 100, 100)',    //Optional
    	    rating:  1, //Optional
    	    minRating:1,    //Optional
    	    readOnly: false, //Optional
    	    callback: function(rating) {    //Mandatory
    	      $scope.ratingsCallback(rating);
    	    }
          };
   			
   		$scope.ratingCallback = function(rating, index) {
	        console.log('this Selected rating is : ', rating, ' and the index is : ');
	      };

   		var RatePopup = $ionicPopup.show({
   			template : '<ionic-ratings style="font-size: 50px;" ratingsobj="ratingsObject"></ionic-ratings>',
   			title: 'Enter your Rating',
   			scope: $scope,
   			buttons: [
		      { text: 'Cancel' },
		      {
		        text: '<b>Submit</b>',
		        type: 'button-balanced',
		        onTap: function(rating, index) {
		            $scope.ratingCallback(rating, index);
		        }
		      }
    		]
   		});	
   	}

	
})

.controller('SubcategoriesCtrl', function($scope, $stateParams, $ionicHistory, Fetch, $ionicLoading, $state) {
	$scope.title = $stateParams.name;

	$ionicLoading.show().then(function() {
		getSubcategories($stateParams.id)	
	});
	
	function getSubcategories(id) {
		Fetch.getSubCategories(id).success(function(data) {
			$scope.items = data.category;
			$ionicLoading.hide();
		}).error(function(error) {
			alert(error);
			$ionicLoading.hide();
		})
	}

	$scope.goBack = function() {
		  $ionicHistory.goBack();
		};

	$scope.showStories =function(catid, subid, name) {
		$state.go("subcategories_stories", {catid : catid, subid: subid,name : name});
	}
	
})

.controller('StoriesCtrl', function($scope, $stateParams, $ionicHistory, $ionicLoading, Fetch) {
	$scope.title = $stateParams.name;

	$scope.goBack = function() {
	      $ionicHistory.goBack();
	   };

	$ionicLoading.show().then(function() {
		getStories($stateParams.catid, $stateParams.subid);
	})

	$scope.doRefresh = function(){
		getStories($stateParams.catid, $stateParams.subid);
	}

	function getStories(catid, subid) {
		Fetch.getStoriesOfSubcategory($stateParams.catid, $stateParams.subid)
			.success(function(data) {
				console.log(data);
				$scope.news = data.story;
				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();
			}).error(function(data) {
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			})
	}
})