// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'ionic-ratings' ])

app.run(function($ionicPlatform, $rootScope, $ionicHistory) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
      StatusBar.backgroundColorByHexString("#4BBE66");
    }
     $rootScope.$on("$locationChangeStart", function(event, next, current) { 
        
    });
  });
})

app.config(function($stateProvider, $urlRouterProvider, Config) {
  $stateProvider
    .state('welcome', {
      url: '/welcome',
      templateUrl: 'templates/welcome.html',
      controller: 'WelcomeCtrl'
    })

    .state('verify', {
      url: '/verify',
      templateUrl: 'templates/verify.html',
      controller: 'VerifyCtrl'
    })

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.latest_news', {
    url: '/latest_news',
    views: {
      'menuContent': {
        templateUrl: 'templates/latest_news.html',
        controller: 'NewsListCtrl as nlc',
      }
    }
  })

  .state('app.categories', {
      url: '/categories',
      views: {
        'menuContent': {
          templateUrl: 'templates/categories.html',
          controller: 'CategoriesCtrl as cctrl'
        }
      }
    })
  
  .state('news_detail', {
    url : '/news_detail/:details',
    templateUrl : 'templates/news_detail.html',
    controller: 'NewsDetailCtrl',
  })
  
  .state('subcategories', {
    url : '/subcategories/:id/:name',
    templateUrl : 'templates/subcategories.html',
    controller: 'SubcategoriesCtrl'
  })

  .state('subcategories_stories', {
    url : '/stories/:catid/:subid/:name',
    templateUrl: 'templates/subcategories_stories.html',
    controller : 'StoriesCtrl'
  })
  
  // if none of the above states are matched, use this as the fallback
  if(localStorage.getItem(Config.localStorageKeyword) == "true")
      $urlRouterProvider.otherwise('/app/latest_news');
    else
      $urlRouterProvider.otherwise('/welcome');
});
